import cv2
import numpy as np
import os
import sys
import math

MAX_IMAGE_SHAPE = 10000

squares_list = []

class ImgSquare:
    SAME_WIDTH = 8
    SAME_HEIGHT = 8
    SAME_AREA = 25
    OFFSET_TOL = 8

    def __init__(self, points):
        assert len(points) == 4
        self.points = points
        self.p1 = points[0]
        self.p2 = points[1]
        self.p3 = points[2]
        self.p4 = points[3]
        self.__x_min = MAX_IMAGE_SHAPE
        self.__x_max = 1
        self.__y_min = MAX_IMAGE_SHAPE
        self.__y_max = 1
        for p in points:
            x = p[0]
            y = p[1]
            if x > self.__x_max:
                self.__x_max = x
            if x < self.__x_min:
                self.__x_min = x
            if y > self.__y_max:
                self.__y_max = y
            if y < self.__y_min:
                self.__y_min = y
        self.width = self.__x_max - self.__x_min
        self.height = self.__y_max - self.__y_min
        self.area = self.width * self.height 
        self.top_left = (self.__x_min, self.__y_min)
        self.top_right = (self.__x_max, self.__y_min)
        self.bottom_right = (self.__x_max, self.__y_max)
        self.bottom_left = (self.__x_min, self.__y_max)
        self.min_x = self.__x_min
        self.max_x = self.__x_max
        self.min_y = self.__y_min
        self.max_y = self.__y_max
        
    def __eq__(self, other):
        eq = True
        if abs(self.width - other.width) > ImgSquare.SAME_WIDTH:
            eq = False
        if abs(self.height - other.height) > ImgSquare.SAME_HEIGHT:
            eq = False
        if abs(self.min_x - other.min_x) > ImgSquare.SAME_WIDTH:
            eq = False
        if abs(self.min_y - other.min_y) > ImgSquare.SAME_HEIGHT:
            eq = False
        #but if the topleft offset between them is below tolerance
        if self.offset_euclid(other) < ImgSquare.OFFSET_TOL:
            eq = True
        return eq
        
    def __ne__(self, other):
        return not self.__eq__(other)

    #sort by top left x pos
    def __gt__(self, other):
        return self.min_x > other.min_x
    
    def __lt__(self, other):
        return self.min_x < other.min_x

    def __ge__(self, other):
        if self.__eq__(other):
            return True
        else:
            return self.__gt__(other)

    def __le__(self, other):
        if self.__eq__(other):
            return True
        else:
            return self.__lt__(other)

    

    def offset_euclid(self, other):
        x_delta = abs(self.top_left[0] - other.top_left[0])
        y_delta = abs(self.top_left[1] - other.top_left[0])
        dist_sq = (x_delta ** 2) + (y_delta ** 2)
        dist = int(math.floor(math.sqrt(dist_sq)))
        return dist

    def delta_points(self, dx, dy):
        ret_points = []
        for p in self.points:
            new_point = (int(p[0] + dx), int(p[1] + dy))
            ret_points.append(new_point)
        return ret_points
        

        
                    


class ImgGrabber:
    #CROP_MIN_X = 150
    #CROP_MIN_Y = 650
    #CROP_MAX_X = 900
    #CROP_MAX_Y = 900

    CROP_MIN_X = 450
    CROP_MIN_Y = 1200
    CROP_MAX_X = 1250
    CROP_MAX_Y = 1500
    
    #mostly from opencv squares example
    @staticmethod
    def angle_cos(p0, p1, p2):
        d1, d2 = (p0-p1).astype('float'), (p2-p1).astype('float')
        return abs( np.dot(d1, d2) / np.sqrt( np.dot(d1, d1)*np.dot(d2, d2) ) )

    @staticmethod
    def read_img(img_path):
        assert os.path.isfile(img_path), "Not a file: {}".format(img_path)
        return cv2.imread(img_path, 0)

    @staticmethod
    def write_img(img_path, img):
        assert os.path.isdir(os.path.dirname(img_path))
        if not cv2.imwrite(img_path, img):
            print "Write Failed for {}!!".format(img_path)
        return
    

    @staticmethod
    def thresh_img(img):
        #thresh_type = cv2.ADAPTIVE_THRESH_GASIAN_C
        ret, thresh = cv2.threshold(img, 200, 255, 0)
        
    
    @staticmethod
    def largest_block_pos(img):
        ret, thresh = cv2.threshold(img, 200, 255, 0)
        contours, hieracrchy = cv2.findContours(thresh, 1, 2)
        cnt = contours[0]
        moments = cv2.moments(cnt)
        cx = int(moments['m10']/moments['m00'])
        cx = int(moments['m10']/moments['m00'])

    #mostly from opencv squares example
    #finds squares in image
    @staticmethod
    def find_squares(img):
        img = cv2.GaussianBlur(img, (5, 5), 0)
        squares = []
        for gray in cv2.split(img):
            for thrs in xrange(0, 255, 26):
                if thrs == 0:
                    bin = cv2.Canny(gray, 0, 50, apertureSize=5)
                    bin = cv2.dilate(bin, None)
                else:
                    retval, bin = cv2.threshold(gray, thrs, 255, cv2.THRESH_BINARY)
                bin, contours, hierarchy = cv2.findContours(bin, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
                for cnt in contours:
                    cnt_len = cv2.arcLength(cnt, True)
                    cnt = cv2.approxPolyDP(cnt, 0.02*cnt_len, True)
                    if len(cnt) == 4 and cv2.contourArea(cnt) > 1000 and cv2.isContourConvex(cnt):
                        cnt = cnt.reshape(-1, 2)
                        max_cos = np.max([ImgGrabber.angle_cos( cnt[i], cnt[(i+1) % 4], cnt[(i+2) % 4] ) for i in xrange(4)])
                        if max_cos < 0.1:
                            squares.append(cnt)
        return squares
    
    #draw square on image
    @staticmethod
    def draw_cv_square(cv_square, img):
        if len(img.shape) == 2:
            b = img
            g = img
            r = img
            img = cv2.merge((b,g,r))
        assert len(square) == 4
        p0 = (cv_square[0][0], cv_square[0][1])
        p1 = (cv_square[1][0], cv_square[1][1])
        p2 = (cv_square[2][0], cv_square[2][1])
        p3 = (cv_square[3][0], cv_square[3][1])
        if (1,1) in p0:
            return img
        elif (1,1) in p1:
            return img
        elif (1,1) in p2:
            return img
        elif (1,1) in p3:
            return img
        img = cv2.line(img, p0, p1, (0, 0, 255))
        img = cv2.line(img, p1, p2, (0, 0, 255))
        img = cv2.line(img, p2, p3, (0, 0, 255))
        img = cv2.line(img, p3, p0, (0, 0, 255))
        return img

    @staticmethod
    def draw_square(square, img):
        if len(img.shape) == 2: 
            b = img
            g = img
            r = img
            img = cv2.merge((b,g,r))
        color = (0,0,255)
        img = cv2.line(img, square.top_left, square.top_right, color)
        img = cv2.line(img, square.top_right, square.bottom_right, color)
        img = cv2.line(img, square.bottom_right, square.bottom_left, color)
        img = cv2.line(img, square.top_left, square.bottom_left, color)
        return img
    
    #crop image to relative position of letter boxes
    @staticmethod
    def crop_img(img):
        assert len(img.shape) == 2
        assert ImgGrabber.CROP_MAX_X < img.shape[1]
        assert ImgGrabber.CROP_MAX_Y < img.shape[0]
        return img[ImgGrabber.CROP_MIN_Y:ImgGrabber.CROP_MAX_Y,\
         ImgGrabber.CROP_MIN_X:ImgGrabber.CROP_MAX_X]

    #convert squares in cropped image to squares in full size image
    @staticmethod
    def crop_sq_to_full(square):
        y_delta = ImgGrabber.CROP_MIN_Y
        x_delta = ImgGrabber.CROP_MIN_X
        p1 = [square.p1[0] + x_delta, square.p1[1] + y_delta]
        p2 = [square.p2[0] + x_delta, square.p2[1] + y_delta]        
        p3 = [square.p3[0] + x_delta, square.p3[1] + y_delta]
        p4 = [square.p4[0] + x_delta, square.p4[1] + y_delta]
        return ImgSquare([p1, p2, p3, p4])

    #convert cv squares to ImgSquares and filter out cv_squares with point (1,1)
    @staticmethod
    def extract_squares(cv_squares):
        square_list = []
        for s in cv_squares:
            orig = [1,1]
            if orig not in s[0] and orig not in s[1]\
             and orig not in s[2] and orig not in s[3]:
                square_list.append(ImgSquare(s))
        return square_list

    #crop image to size of full 8 blocks
    @staticmethod
    def full_list(squares, img):
        (xl, xr, yt, yb) = ImgGrabber.squares_range(squares)
        return img[yt:yb, xl:xr]

    #list of ImgSquares and finds left/right x and top/bottom y
    @staticmethod
    def squares_range(squares):
        xl = MAX_IMAGE_SHAPE
        yt = MAX_IMAGE_SHAPE
        xr = -1
        yb = -1
        for s in squares:
            if s.min_x < xl:
                xl = s.min_x
            if s.min_y < yt:
                yt = s.min_y
            if s.max_x > xr:
                xr = s.max_x
            if s.max_y > yb:
                yb = s.max_y
        return (xl, xr, yt, yb)

    #crops image to square (ImgSquare)
    @staticmethod
    def get_square(square, img):
        return img[square.min_y:square.max_y, square.min_x:square.max_x]

    #try to grab approx box with num for hash
    @staticmethod
    def get_hash(squares, img):
        x_delta = 210
        y_delta = -450
        x_range = 100
        y_range = -55
        (xl, xr, yt, yb) = ImgGrabber.squares_range(squares)
        assert (yt + y_delta + y_range) > 0
        assert (xr + x_delta + x_range) < img.shape[1]
        p1 = [xr + x_delta, yt + y_delta + y_range]
        p2 = [xr + x_delta + x_range, yt + y_delta + y_range]
        p3 = [xr + x_delta + x_range, yt + y_delta]
        p4 = [xr + x_delta, yt + y_delta]
        sq = ImgSquare([p1, p2, p3, p4])
        return ImgGrabber.get_square(sq, img)

    #could be faster but...
    #return list of unique ImgSquares
    @staticmethod
    def combine_squares(squares):
        unique_squares = []
        for s in squares:
            if s not in unique_squares:
                unique_squares.append(s)
        return unique_squares

    #filter list of squares based on size
    @staticmethod
    def filter_size(squares):
        #h_min = 35
        #h_max = 55
        #w_min = 35
        #w_max = 45
        h_min = 80
        h_max = 120
        w_min = 60
        w_max = 90        
        remaining_squares = []
        for s in squares:
            if s.height > h_min and s.height < h_max \
              and s.width > w_min and s.width < w_max:
                remaining_squares.append(s)
        return remaining_squares
            
            


def main_folder(folder):
    files = os.listdir(folder)
    for f in files:
        main_file(os.path.join(folder, f))
 


def main_file(input):
    #Averages - TLX: 184.815157116 TLY: 92.8373382625 W: 40.4417744917 H: 47.0961182994
    input_img_path = input
    img = ImgGrabber.read_img(input_img_path)
    img_crop = ImgGrabber.crop_img(img)
    crop_path = input_img_path + '_crop.tif'
    ImgGrabber.write_img(crop_path, img_crop)
    cv_squares = ImgGrabber.find_squares(img_crop)
    squares = ImgGrabber.extract_squares(cv_squares)
    cmb_sqs = ImgGrabber.combine_squares(squares)
    filt_sqs = ImgGrabber.filter_size(cmb_sqs)
    
    print "found length {}, combined length {}, filtered length {}".format(len(squares), len(cmb_sqs), len(filt_sqs))
    if len(filt_sqs) == 8:
        sorted_sqs = sorted(filt_sqs)
        #squares_list.append((sorted_sqs[0].top_left, sorted_sqs[0].width, sorted_sqs[0].height))
        img_all = ImgGrabber.full_list(sorted_sqs, img_crop)
        out_path_all = input_img_path + '_all.tif'
        ImgGrabber.write_img(out_path_all, img_all)
        for e in enumerate(sorted_sqs):
            s = e[1]
            i = e[0]
            out_path = input_img_path + '_{}.tif'.format(i + 1)
            img_sq = ImgGrabber.get_square(s, img_crop)
            ImgGrabber.write_img(out_path, img_sq)
        #full_sqs = []
        #for s in sorted_sqs:
        #    full_sqs.append(ImgGrabber.crop_sq_to_full(s))
        #img_hash = ImgGrabber.get_hash(full_sqs, img)
        #out_path_hash = input_img_path + '_hash.tif'
        #ImgGrabber.write_img(out_path_hash, img_hash)
    else:
        #alternative processing, still needs work
        '''
        squares = average_squares()
        sorted_sqs = sorted(squares)
        img_all = ImgGrabber.full_list(sorted_sqs, img_crop)
        out_path_all = input_img_path + '_all.tif'
        ImgGrabber.write_img(out_path_all, img_all)
        for e in enumerate(sorted_sqs):
            s = e[1]
            i = e[0]
            out_path = input_img_path + '_{}.tif'.format(i + 1)
            img_sq = ImgGrabber.get_square(s, img_crop)
            ImgGrabber.write_img(out_path, img_sq)
        '''      
        #print "Processing failed for file {}".format(input)
        


def average_square(squares_list):
    total_num = float(len(squares_list))
    tot_x = 0
    tot_y = 0
    tot_w = 0
    tot_h = 0        
    for t in squares_list:
        tot_x += t[0][0]
        tot_y += t[0][1]
        tot_w += t[1]
        tot_h += t[2]
        
    avg_x = float(tot_x) / total_num
    avg_y = float(tot_y) / total_num    
    avg_w = float(tot_w) / total_num
    avg_h = float(tot_h) / total_num
    print "Averages - X: {} Y: {} W: {} H: {}".format(avg_x, avg_y, avg_w, avg_h)

def average_squares():
    #Averages - TLX: 184.815157116 TLY: 92.8373382625 W: 40.4417744917 H: 47.0961182994
    avg_x = 184.815157116
    avg_y = 92.8373382625
    avg_w = 40.4417744917
    avg_h = 47.0961182994
    p1 = (int(avg_x), int(avg_y))
    p2 = (int(avg_x + avg_w), int(avg_y))
    p3 = (int(avg_x + avg_w), int(avg_y + avg_h))
    p4 = (int(avg_x), int(avg_y + avg_h))
    s1 = ImgSquare([p1, p2, p3, p4])
    squares = []
    squares.append(s1)
    for i in range(2,9):
        cur_w = avg_w * i
        s = ImgSquare(s1.delta_points(cur_w, 0))
        squares.append(s)
    return squares


            
def main():
    if os.path.isfile(sys.argv[1]):
        main_file(sys.argv[1])
    elif os.path.isdir(sys.argv[1]):
        main_folder(sys.argv[1])
    #if len(squares_list) > 0:
    #    average_square(squares_list)




    
        

if __name__ == '__main__':
    main()






